package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int button=1,max=0,min=0,count=0;

        while (button!=0)
        {
            System.out.println("Input action: 1 - up, 2 - down, 0 - exit");
            button=scanner.nextInt();
            if (button==1)
            {
                count++;
                if (count>max)
                {
                    max=count;
                }
            }
            else if (button==2)
            {
                count--;
                if (count<min)
                {
                    min=count;
                }
            }
        }
        System.out.println(max-min+1);
    }
}
